## How to set up custom logs

Data in log file can be saved in table format. In Magento admin you can assign formatters to table columns:

1. Go to _System > Tools > Log Viewer_
2. On the top of the page click on _Manage column formatters_
3. Click on _Add New Column Formatter_

**Form description:**
* **Index**: number coresponding to the array index of data when creating new log entry.
* **Name**: heading for log column on this index. 
* **Log File**: log file path relative to var/log for which column formatter applies.
* **Formatter Class**: Class used to format this column.

**Formatter Classes:**
* _StringData_: Data is converted to string.
* _ExceptionData_: Data must implement \Throwable interface. Saves Tracy bluescreen for Exception.
* _StructuredData_: Object or array. It is saved in in browserable format.

---

## Using custom logger
You need to add LogViewer Logger to dependancy injection, for example:
```php
public function __construct(
	...
	\Magezone\LogViewer\Logger\Logger $logger
	...
)
...
```

Logged data must be an array. Each array value corresponds to one column in log view. 
If column formatter for selected log file and column index is not specified, 
StringData formatter is used. Number of array values must be constant for a log file.
Logging levels are same as in default Monolog logger.

Exception logging example:
```php
...
catch (\Throwable $e) {
	// in Magento admin, there is ExceptionData column formatter for file test.log and index 2
	$this->logger->warning('test.log', ['test log data', 'second column', $e]); 
}
...
``` 

Structured data example:
```php
...
// in Magento admin, there is StructuredData column formatter for file test2.log and index 1
$data = ['person' => ['name' => 'Mikhael', 'age' => 24]];
$this->logger->debug('test2.log', ['some column', $data, 'other column']);
...
```