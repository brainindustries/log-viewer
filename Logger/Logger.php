<?php

namespace Magezone\LogViewer\Logger;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magezone\LogViewer\Api\Log\ColumnRepositoryInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Logger\Monolog as MonoLogger;

/**
 * Class Logger
 * @package Magezone\LogViewer\Logger
 */
class Logger implements LoggerInterface
{

	const DATE_LEVEL_PATTERN = '@^\[(.*?)\](.*?)\.(.*?):@';

	const COLUMN_SEPARATOR = '|||';

	const DEFAULT_FORMATTER = \Magezone\LogViewer\Block\Adminhtml\Column\Formatter\StringData::class;

	protected $handlers;

	protected $query = null;

	protected $columns = [];

	/**
	 * @var MonoLogger
	 */
	protected $defaultLogger;

	/**
	 * @var DirectoryList
	 */
	protected $directoryList;

	/**
	 * @var ObjectManagerInterface
	 */
	protected $objectManager;

	/**
	 * @var ColumnRepositoryInterface
	 */
	protected $columnRepository;

	/**
	 * Logger constructor.
	 * @param string $name
	 * @param array $handlers
	 * @param array $processors
	 * @param ObjectManagerInterface $objectManager
	 * @param DirectoryList $directoryList
	 * @param ColumnRepositoryInterface $columnRepository
	 * @param MonoLogger $defaultLogger
	 */
	public function __construct(
		string $name,
		$handlers = array(),
		$processors = array(),
		ObjectManagerInterface $objectManager,
		DirectoryList $directoryList,
		ColumnRepositoryInterface $columnRepository,
		MonoLogger $defaultLogger
	)
	{
		$this->handlers = $handlers;
		$this->objectManager = $objectManager;
		$this->directoryList = $directoryList;
		$this->columnRepository = $columnRepository;
		$this->defaultLogger = $defaultLogger;
//		parent::__construct($name, $handlers, $processors);
	}

	/**
	 * @param $fileName
	 * @param $data
	 * @param $level
	 * @param array $context
	 * @return bool
	 */
	public function prepareRecord($fileName, $data, $level, array $context = array())
	{
		$logFile = $this->directoryList->getPath(DirectoryList::LOG) . DIRECTORY_SEPARATOR . $fileName;

		if (count($this->handlers)) {
			foreach ($this->handlers as $handler) {
				if ($handler instanceof Handler) {
					$handler->setLogFile($logFile);
//					print_r($handler);
				}
			}
		}
		$this->defaultLogger->setHandlers($this->handlers);

		if (is_array($data) || is_object($data)) {
			$message = $this->parseData($fileName, $data);
		} else {
			$message = $data;
		}

		return $this->defaultLogger->addRecord($level, $message, $context);
	}

	/**
	 * @param $fileName
	 * @param $data
	 * @param bool $serialize
	 * @return string|array
	 */
	public function parseData($fileName, $data, $serialize = true)
	{

		if (!array_key_exists($fileName, $this->columns)) {
			$this->loadColumns($fileName);
		}

		$columns = $this->columns[$fileName];
		$parsedData = $matches = [];
//		ksort($data);

		preg_match(self::DATE_LEVEL_PATTERN, $data[0], $matches);
		$data[0] = trim(preg_replace(self::DATE_LEVEL_PATTERN, '', $data[0]));

		$formatters = [];
		foreach ($data as $index => $value) {
			$index = (string)$index;
			$formatterClass = self::DEFAULT_FORMATTER;
			if (array_key_exists($index, $columns)) {
				$formatterClass = $columns[$index]->getFormatter();

			}

			if (!array_key_exists($formatterClass, $formatters)) {
				$formatter = $this->objectManager->create($formatterClass);
				$formatters[$formatterClass] = $formatter;
			}

			$formatter = $formatters[$formatterClass];
			if (is_object($formatter) && $formatter instanceof \Magezone\LogViewer\Block\Adminhtml\Column\FormatterInterface) {
				if ($serialize) {
					$parsedData[$index] = $formatter->serializeColumn($value, $fileName);
				} else {
					$parsedData[$index] = $formatter->unserializeColumn($value, $this->query);
				}
			} else {
				$parsedData[$index] = (string)$value;
			}
		}

		if (!empty($matches)) {
			array_unshift($parsedData, $matches[1], $matches[3]);
		}

		return ($serialize) ? implode(self::COLUMN_SEPARATOR, $parsedData) . self::COLUMN_SEPARATOR : $parsedData;
	}

	public function getColumnLabels($fileName)
	{
		if (!array_key_exists($fileName, $this->columns)) {
			$this->loadColumns($fileName);
		}

		$labels = ['Date', 'Level'];
		foreach ($this->columns[$fileName] as $column) {
			$labels[$column->getIndex() + 2] = $column->getName();
		}

		return $labels;
	}

	protected function loadColumns($fileName)
	{
		$this->columns[$fileName] = [];
		$columns = $this->columnRepository->getByLog($fileName);
		if ($columns->count()) {
			foreach ($columns as $column) {
				$this->columns[$fileName][$column->getIndex()] = $column;
			}
		}
	}

	/**
	 * System is unusable.
	 *
	 * @param $fileName
	 * @param $data
	 * @param array $context
	 *
	 * @return void
	 */
	public function emergency($fileName, $data, array $context = array())
	{
		$this->prepareRecord($fileName, $data, MonoLogger::EMERGENCY, $context);
	}

	/**
	 * Action must be taken immediately.
	 *
	 * Example: Entire website down, database unavailable, etc. This should
	 * trigger the SMS alerts and wake you up.
	 *
	 * @param $fileName
	 * @param $data
	 * @param array $context
	 *
	 * @return void
	 */
	public function alert($fileName, $data, array $context = array())
	{
		$this->prepareRecord($fileName, $data, MonoLogger::EMERGENCY, $context);
	}

	/**
	 * Critical conditions.
	 *
	 * Example: Application component unavailable, unexpected exception.
	 *
	 * @param $fileName
	 * @param $data
	 * @param array $context
	 *
	 * @return void
	 */
	public function critical($fileName, $data, array $context = array())
	{
		$this->prepareRecord($fileName, $data, MonoLogger::EMERGENCY, $context);
	}

	/**
	 * Runtime errors that do not require immediate action but should typically
	 * be logged and monitored.
	 *
	 * @param $fileName
	 * @param $data
	 * @param array $context
	 *
	 * @return void
	 */
	public function error($fileName, $data, array $context = array())
	{
		$this->prepareRecord($fileName, $data, MonoLogger::ERROR, $context);
	}

	/**
	 * Exceptional occurrences that are not errors.
	 *
	 * Example: Use of deprecated APIs, poor use of an API, undesirable things
	 * that are not necessarily wrong.
	 *
	 * @param $fileName
	 * @param $data
	 * @param array $context
	 *
	 * @return void
	 */
	public function warning($fileName, $data, array $context = array())
	{
		$this->prepareRecord($fileName, $data, MonoLogger::WARNING, $context);
	}

	/**
	 * Normal but significant events.
	 *
	 * @param $fileName
	 * @param $data
	 * @param array $context
	 *
	 * @return void
	 */
	public function notice($fileName, $data, array $context = array())
	{
		$this->prepareRecord($fileName, $data, MonoLogger::NOTICE, $context);
	}

	/**
	 * Interesting events.
	 *
	 * Example: User logs in, SQL logs.
	 *
	 * @param $fileName
	 * @param $data
	 * @param array $context
	 *
	 * @return void
	 */
	public function info($fileName, $data, array $context = array())
	{
		$this->prepareRecord($fileName, $data, MonoLogger::INFO, $context);
	}

	/**
	 * Detailed debug information.
	 *
	 * @param $fileName
	 * @param $data
	 * @param array $context
	 *
	 * @return void
	 */
	public function debug($fileName, $data, array $context = array())
	{
		$this->prepareRecord($fileName, $data, MonoLogger::DEBUG, $context);
	}

	/**
	 * Logs with an arbitrary level.
	 *
	 * @param mixed $level
	 * @param $fileName
	 * @param $data
	 * @param array $context
	 *
	 * @return void
	 */
	public function log($level, $fileName, $data, array $context = array())
	{
		$this->prepareRecord($fileName, $data, $level, $context);
	}

	/**
	 * @param $query
	 */
	public function setQuery($query)
	{
		$this->query = $query;
	}
}