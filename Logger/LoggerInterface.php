<?php
namespace Magezone\LogViewer\Logger;

/**
 * Interface LoggerInterface
 */
interface LoggerInterface
{

	/**
	 * System is unusable.
	 *
	 * @param $fileName
	 * @param $data
	 * @param array $context
	 *
	 * @return void
	 */
	public function emergency($fileName, $data, array $context = array());

	/**
	 * Action must be taken immediately.
	 *
	 * Example: Entire website down, database unavailable, etc. This should
	 * trigger the SMS alerts and wake you up.
	 *
	 * @param $fileName
	 * @param $data
	 * @param array $context
	 *
	 * @return void
	 */
	public function alert($fileName, $data, array $context = array());

	/**
	 * Critical conditions.
	 *
	 * Example: Application component unavailable, unexpected exception.
	 *
	 * @param $fileName
	 * @param $data
	 * @param array $context
	 *
	 * @return void
	 */
	public function critical($fileName, $data, array $context = array());

	/**
	 * Runtime errors that do not require immediate action but should typically
	 * be logged and monitored.
	 *
	 * @param $fileName
	 * @param $data
	 * @param array $context
	 *
	 * @return void
	 */
	public function error($fileName, $data, array $context = array());

	/**
	 * Exceptional occurrences that are not errors.
	 *
	 * Example: Use of deprecated APIs, poor use of an API, undesirable things
	 * that are not necessarily wrong.
	 *
	 * @param $fileName
	 * @param $data
	 * @param array $context
	 *
	 * @return void
	 */
	public function warning($fileName, $data, array $context = array());

	/**
	 * Normal but significant events.
	 *
	 * @param $fileName
	 * @param $data
	 * @param array $context
	 *
	 * @return void
	 */
	public function notice($fileName, $data, array $context = array());

	/**
	 * Interesting events.
	 *
	 * Example: User logs in, SQL logs.
	 *
	 * @param $fileName
	 * @param $data
	 * @param array $context
	 *
	 * @return void
	 */
	public function info($fileName, $data, array $context = array());

	/**
	 * Detailed debug information.
	 *
	 * @param $fileName
	 * @param $data
	 * @param array $context
	 *
	 * @return void
	 */
	public function debug($fileName, $data, array $context = array());

	/**
	 * Logs with an arbitrary level.
	 *
	 * @param mixed $level
	 * @param $fileName
	 * @param $data
	 * @param array $context
	 *
	 * @return void
	 */
	public function log($level, $fileName, $data, array $context = array());
}