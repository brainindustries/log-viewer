<?php
namespace Magezone\LogViewer\Logger;

use Monolog\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{
	/**
	 * Logging level
	 * @var int
	 */
	protected $loggerType = Logger::DEBUG;

	/**
	 * File name
	 * @var string
	 */
	protected $fileName = '/var/log/logviewer.log';


	public function setLogFile($fileName)
	{
		$this->fileName = $fileName;
		$this->url = $fileName;
	}

}