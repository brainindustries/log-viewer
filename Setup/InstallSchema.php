<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 7.5.2018
 * Time: 8:49
 */

namespace Magezone\LogViewer\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
	/**
	 *
	 *
	 * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
	 * @param \Magento\Framework\Setup\ModuleContextInterface $context
	 * @return void
	 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
	 * @throws \Zend_Db_Exception
	 */
	public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
		$installer = $setup;
		$installer->startSetup();


		$tableName = 'magezone_logviewer_log_column';
		if (!$installer->tableExists($tableName)) {
			$table = $installer->getConnection()->newTable(
				$installer->getTable($tableName)
			)
				->addColumn(
					'id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					null,
					[
						'identity' => true,
						'nullable' => false,
						'primary'  => true,
						'unsigned' => true,
					],
					'ID'
				)
				->addColumn(
					'log_file',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					['nullable => false']
				)
				->addColumn(
					'name',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					['nullable' => false]
				)
				->addColumn(
					'index',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					11,
					['nullable' => false]
				)
				->addColumn(
					'formatter',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					['nullable' => false]
				);

			$installer->getConnection()->createTable($table);

			$installer->getConnection()->addIndex(
				$installer->getTable($tableName),
				$setup->getIdxName(
					$installer->getTable($tableName), ['log_file'],
					\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_INDEX
				),
				['log_file'],
				\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_INDEX
			);

		}


		$installer->endSetup();
	}
}