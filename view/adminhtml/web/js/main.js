function myFunction() {
	var input, filter, table, tr, td, i;
	input = document.getElementById("myInput");
	filter = input.value.toUpperCase();
	table = document.getElementById("table-logs");
	tr = table.getElementsByTagName("tr");
	for (i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td")[3];
		if (td) {
			if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
				tr[i].style.display = "";
			} else {
				tr[i].style.display = "none";
			}
		}
	}
}

require([
	'jquery',
	'accordion'  // the alias for "mage/accordion"
], function ($) {
	$(function () {
		var $tbl = $("#table-logs");

		// $(document).on('click', '#grpChkBox input:checkbox', function () {
		// 	var colToHide = $tbl.find("." + $(this).attr("name"));
		// 	$(colToHide).toggle();
		// });
	});
});


