<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Used in creating options for Yes|No config value selection
 *
 */

namespace Magezone\LogViewer\Model\Config\Source;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Driver\File;

/**
 * Class Formatters
 * @package Magezone\LogViewer\Model\Config\Source
 */
class Logfiles implements \Magento\Framework\Option\ArrayInterface
{


	/**
	 * @var \Magento\Framework\App\Filesystem\DirectoryList
	 */
	protected $directoryList;

	/**
	 * @var \Magento\Framework\Filesystem\Driver\File $file
	 */
	protected $driverFile;

	/**
	 * Constructor
	 *
	 * @param \Magento\Framework\App\Filesystem\DirectoryList $directoryList
	 * @param \Magento\Framework\Filesystem\Driver\File $driverFile
	 */
	public function __construct(
		DirectoryList $directoryList,
		File $driverFile
	)
	{
		$this->directoryList = $directoryList;
		$this->driverFile = $driverFile;
	}

	/**
	 * Options getter
	 *
	 * @return array
	 */
	public function toOptionArray()
	{
		$files = $this->toArray();
		$options = [];
		foreach ($files as $value => $label) {
			$options[] = ['value' => $value, 'label' => $label];
		}
		return $options;
	}

	/**
	 * Get options in "key-value" format
	 *
	 * @return array
	 */
	public function toArray()
	{
		$logPath = $this->directoryList->getPath(DirectoryList::LOG);
		return $this->_getLogFiles($logPath);
	}

	/**
	 * @param $dir
	 * @param array $logFiles
	 * @return array
	 */
	protected function _getLogFiles($dir, &$logFiles = [])
	{
		try {
			$logPath = $this->directoryList->getPath(DirectoryList::LOG) . DIRECTORY_SEPARATOR;
			$driverFiles = $this->driverFile->readDirectory($dir);

			foreach ($driverFiles as $key => $path) {
				$name = basename($path);
				if (!is_dir($path)) {
					if (substr($name, -5) != '.html') {
						$relativePath = str_replace($logPath, '', $path);
						$logFiles[$relativePath] = $relativePath;
					}
				} else if ($name != "." && $name != "..") {
					$this->_getLogFiles($path, $logFiles);
				}
			}
		} catch (\Throwable $e) {
			echo $e->getMessage();
		}
		return $logFiles;
	}
}
