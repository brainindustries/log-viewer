<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Used in creating options for Yes|No config value selection
 *
 */

namespace Magezone\LogViewer\Model\Config\Source;

/**
 * Class Formatters
 * @package Magezone\LogViewer\Model\Config\Source
 */
class Formatters implements \Magento\Framework\Option\ArrayInterface
{

	protected $formatters = [];

	/**
	 * Formatters constructor.
	 * @param array $formatters
	 */
	public function __construct(
		array $formatters = []
	)
	{
		$this->formatters = $formatters;
	}

	/**
	 * Options getter
	 *
	 * @return array
	 */
	public function toOptionArray()
	{
		$formatters = $this->toArray();
		$options = [];
		foreach ($formatters as $value => $label) {
			$options[] = ['value' => $value, 'label' => $label];
		}
		return $options;

	}

	/**
	 * Get options in "key-value" format
	 *
	 * @return array
	 */
	public function toArray()
	{
		$formatterClasses = array_values($this->formatters);
		return array_combine($formatterClasses, $formatterClasses);
	}
}
