<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 7.5.2018
 * Time: 8:48
 */

namespace Magezone\LogViewer\Model\ResourceModel\Log;

class Column extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

	protected function _construct()
	{
		$this->_init('magezone_logviewer_log_column', 'id');
	}

}