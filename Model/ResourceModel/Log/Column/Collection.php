<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 7.5.2018
 * Time: 8:48
 */

namespace Magezone\LogViewer\Model\ResourceModel\Log\Column;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

	/**
	 * @var string
	 */
	protected $_idFieldName = 'id';


	protected function _construct()
	{
		$this->_init('Magezone\LogViewer\Model\Log\Column', 'Magezone\LogViewer\Model\ResourceModel\Log\Column');
	}

}