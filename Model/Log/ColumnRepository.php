<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magezone\LogViewer\Model\Log;

use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magezone\LogViewer\Api\Log\ColumnRepositoryInterface;
use Magezone\LogViewer\Model\ResourceModel\Log\Column as ResourceColumn;
use Magento\Framework\Exception\CouldNotSaveException;
use \Magezone\LogViewer\Model\Log\ColumnFactory;
use Magezone\LogViewer\Model\ResourceModel\Log\Column\CollectionFactory as ColumnCollectionFactory;

/**
 * Class PageRepository
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ColumnRepository implements ColumnRepositoryInterface
{

	protected $resource;

	/**
	 * @var ColumnFactory
	 */
	protected $columnFactory;

	/**
	 * @var ColumnCollectionFactory
	 */
	protected $columnCollectionFactory;

	public function __construct(
		ResourceColumn $resource,
		ColumnFactory $columnFactory,
		ColumnCollectionFactory $columnCollectionFactory
	)
	{
		$this->resource = $resource;
		$this->columnFactory = $columnFactory;
		$this->columnCollectionFactory = $columnCollectionFactory;
	}

	/**
	 * Save column.
	 *
	 * @param \Magezone\LogViewer\Api\Data\Log\ColumnInterface $column
	 * @return \Magezone\LogViewer\Api\Data\Log\ColumnInterface
	 * @throws CouldNotSaveException
	 */
	public function save(\Magezone\LogViewer\Api\Data\Log\ColumnInterface $column)
	{
		try {
			$this->resource->save($column);
		} catch (\Exception $exception) {
			throw new CouldNotSaveException(
				__('Could not save the page: %1', $exception->getMessage()),
				$exception
			);
		}
		return $column;
	}


	/**
	 * Retrieve column.
	 *
	 * @param string $logName
	 * @return \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
	 */
	public function getByLog($logName)
	{
		/**
		 * @var $columnCollection \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
		 */
		$columnCollection = $this->columnCollectionFactory->create();
		return $columnCollection->addFieldToFilter('log_file', $logName);
	}

	/**
	 * Delete column.
	 *
	 * @param \Magezone\LogViewer\Api\Data\Log\ColumnInterface $column
	 * @return bool true on success
	 * @throws \Magento\Framework\Exception\LocalizedException
	 */
	public function delete(\Magezone\LogViewer\Api\Data\Log\ColumnInterface $column)
	{
		try {
			$this->resource->delete($column);
		} catch (\Exception $exception) {
			throw new CouldNotDeleteException(__(
				'Could not delete the column: %1',
				$exception->getMessage()
			));
		}
		return true;
	}

	/**
	 * Load Page data by given Page Identity
	 *
	 * @param string $columnId
	 * @return Column
	 * @throws \Magento\Framework\Exception\NoSuchEntityException
	 */
	public function getById($columnId)
	{
		$column = $this->columnFactory->create();
		$column->load($columnId);
		if (!$column->getId()) {
			throw new NoSuchEntityException(__('Column with id "%1" does not exist.', $columnId));
		}
		return $column;
	}

	/**
	 * Delete column by ID.
	 *
	 * @param int $columnId
	 * @return bool true on success
	 * @throws \Magento\Framework\Exception\NoSuchEntityException
	 * @throws \Magento\Framework\Exception\LocalizedException
	 */
	public function deleteById($columnId)
	{
		return $this->delete($this->getById($columnId));
	}
}
