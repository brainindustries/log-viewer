<?php

namespace Magezone\LogViewer\Model\Log\Column;
use Magezone\LogViewer\Model\ResourceModel\Log\Column\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{

	/**
	 * @param string $name
	 * @param string $primaryFieldName
	 * @param string $requestFieldName
	 * @param CollectionFactory $columnCollectionFactory
	 * @param array $meta
	 * @param array $data
	 */
	public function __construct(
		$name,
		$primaryFieldName,
		$requestFieldName,
		CollectionFactory $columnCollectionFactory,
		array $meta = [],
		array $data = []
	) {
		$this->collection = $columnCollectionFactory->create();
		parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
	}

	public function getData()
	{
		if (isset($this->loadedData)) {
			return $this->loadedData;
		}

		$items = $this->collection->getItems();
		$this->loadedData = [];

		foreach ($items as $column) {
			$this->loadedData[$column->getId()]['column'] = $column->getData();
		}


		return $this->loadedData;

	}
}