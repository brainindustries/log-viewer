<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 7.5.2018
 * Time: 8:48
 */

namespace Magezone\LogViewer\Model\Log;

/**
 * @method \Magezone\LogViewer\Model\ResourceModel\Log\Column getResource()
 * @method \Magezone\LogViewer\Model\ResourceModel\Log\Column\Collection getCollection()
 */
class Column extends \Magento\Framework\Model\AbstractModel implements \Magezone\LogViewer\Api\Data\Log\ColumnInterface,
	\Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'magezone_logviewer_column';

	protected $_cacheTag = 'magezone_logviewer_column';

	protected $_eventPrefix = 'magezone_logviewer_column';

	protected function _construct()
	{
		$this->_init('Magezone\LogViewer\Model\ResourceModel\Log\Column');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}
}