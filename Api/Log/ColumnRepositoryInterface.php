<?php

namespace Magezone\LogViewer\Api\Log;

/**
 * Log Column CRUD interface.
 * @api
 * @since 100.0.2
 */
interface ColumnRepositoryInterface
{
	/**
	 * Save column.
	 *
	 * @param \Magezone\LogViewer\Api\Data\Log\ColumnInterface $column
	 * @return \Magezone\LogViewer\Api\Data\Log\ColumnInterface
	 */
    public function save(\Magezone\LogViewer\Api\Data\Log\ColumnInterface $column);

    /**
     * Retrieve column.
     *
     * @param int $columnId
     * @return \Magezone\LogViewer\Api\Data\Log\ColumnInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($columnId);

	/**
	 * Retrieve column.
	 *
	 * @param string $logName
	 * @return \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
	 */
	public function getByLog($logName);

    /**
     * Delete column.
     *
     * @param \Magezone\LogViewer\Api\Data\Log\ColumnInterface $column
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Magezone\LogViewer\Api\Data\Log\ColumnInterface $column);

    /**
     * Delete column by ID.
     *
     * @param int $columnId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($columnId);
}
