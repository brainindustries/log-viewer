<?php
namespace Magezone\LogViewer\Controller\Adminhtml\Logs;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magezone\LogViewer\Logger\Logger;
use Magezone\LogViewer\Model\Config\Source\Logfiles;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Filesystem\Driver\File;

/**
 * Class Index
 * @package Magezone\LogViewer\Controller\Adminhtml\Logs
 */
class View extends \Magento\Backend\App\Action
{

	/**
	 * @var \Magezone\LogViewer\Logger\Logger
	 */
	protected $logger;

	/**
	 * @var \Magento\Framework\App\Filesystem\DirectoryList
	 */
	protected $directoryList;

	/**
	 * @var \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
	 */
	protected $jsonFactory;

	/**
	 * @var array $jsonFactory
	 */
	protected $logFiles;

	/**
	 * @var \Magento\Framework\Filesystem\Driver\File $file
	 */
	protected $driverFile;

	/**
	 * Constructor
	 *
	 * @param \Magento\Backend\App\Action\Context $context
	 * @param \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
	 * @param \Magezone\LogViewer\Model\Config\Source\Logfiles $logFiles
	 * @param \Magento\Framework\App\Filesystem\DirectoryList $directoryList
	 * @param \Magento\Framework\Filesystem\Driver\File $driverFile
	 * @param Logger $logger
	 */
	public function __construct(
		Context $context,
		JsonFactory $jsonFactory,
		Logfiles $logFiles,
		DirectoryList $directoryList,
		File $driverFile,
		Logger $logger

	)
	{
		parent::__construct($context);
		$this->jsonFactory = $jsonFactory;
		$this->directoryList = $directoryList;
		$this->logFiles = $logFiles;
		$this->driverFile = $driverFile;
		$this->logger = $logger;
	}

	/**
	 * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
	 * @throws \Magento\Framework\Exception\FileSystemException
	 */
	public function execute()
	{
		$response = $matches = [];
		$response['files'] = $this->logFiles->toOptionArray();

		$query = $this->getRequest()->getParam('query');
		$page = $this->getRequest()->getParam('page', 1);
		$count = $this->getRequest()->getParam('count', 20);
		$currentFile = $this->getRequest()->getParam('file', $response['files'][0]['value']);

		$this->logger->setQuery($query);
		$filePath = $this->directoryList->getPath(DirectoryList::LOG) . DIRECTORY_SEPARATOR . $currentFile;
		if (file_exists($filePath) && is_file($filePath)) {
			$file = $this->driverFile->fileOpen($filePath, 'r');

			$readStart = ($page - 1) * $count;
			$readEnd = $readStart + $count;

			$response['columns']['count'] = 0;
			for ($i = 0; $i < $readEnd; $i++) {
				$line = $this->rfgets($file);
				if (empty($line)) break;
				if ($i < $readStart) continue;

				if (!empty($query) && strpos($line, $query) === FALSE) {
					$i--; // ignore line
					continue;
				}

				$columnValues = explode(Logger::COLUMN_SEPARATOR, $line);
				$columnValues = $this->logger->parseData($currentFile, $columnValues, false);

				$response['columns']['count'] = max($response['columns']['count'], count($columnValues));
				$response['entries'][] = $columnValues;
			}
			$response['columns']['labels'] = $this->logger->getColumnLabels($currentFile);
			$response['totalCount'] = $this->getLineCount($filePath);
			$this->driverFile->fileClose($file);
		}

		$response['currentFile'] = $currentFile;
		return $this->jsonFactory->create()->setData($response);
	}

	/**
	 * @param $handle
	 * @return string
	 * @throws \Magento\Framework\Exception\FileSystemException
	 */
	protected function rfgets($handle)
	{
		$line = null;
		if ($handle) {
			$line = '';
			$started = false;
			$gotline = false;
			while (!$gotline) {
				if ($this->driverFile->fileTell($handle) == 0) {
					fseek($handle, -1, SEEK_END);
//					$this->driverFile->fileSeek($handle, -1, SEEK_END);
				} else {
					fseek($handle, -2, SEEK_CUR);
//					$this->driverFile->fileSeek($handle, -2, SEEK_CUR);
				}

				$readres = ($char = fgetc($handle));

				if (false === $readres) {
					$gotline = true;
				} elseif ($char == "\n" || $char == "\r") {
					if ($started)
						$gotline = true;
					else
						$started = true;
				} elseif ($started) {
					$line .= $char;
				}
			}
		}

		$this->driverFile->fileSeek($handle, 1, SEEK_CUR);

		return strrev($line);
	}

	protected function getLineCount($filePath)
	{
		$file = new \SplFileObject($filePath, 'r');
		$file->seek(PHP_INT_MAX);
		return $file->key() + 1;
	}

	protected function _isAllowed()
	{
		return true;
	}
}

?>
