<?php
namespace Magezone\LogViewer\Controller\Adminhtml\Logs;

use Magento\Framework\Controller\Result\RawFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Filesystem\Driver\File;

/**
 * Class Index
 * @package Magezone\LogViewer\Controller\Adminhtml\Logs
 */
class Exception extends \Magento\Backend\App\Action
{


	/**
	 * @var \Magento\Framework\Controller\Result\RawFactory $rawFactory
	 */
	protected $rawFactory;

	/**
	 * @var \Magento\Framework\Filesystem\Driver\File $file
	 */
	protected $driverFile;

	/**
	 * Constructor
	 *
	 * @param \Magento\Backend\App\Action\Context $context
	 * @param \Magento\Framework\Controller\Result\RawFactory $rawFactory
	 * @param \Magento\Framework\Filesystem\Driver\File $driverFile
	 */
	public function __construct(
		Context $context,
		RawFactory $rawFactory,
		File $driverFile

	)
	{
		parent::__construct($context);
		$this->rawFactory = $rawFactory;
		$this->driverFile = $driverFile;
	}

	/**
	 * @return \Magento\Framework\App\ResponseInterface|
	 * @throws \Magento\Framework\Exception\FileSystemException
	 */
	public function execute()
	{
		$exceptionFile = urldecode($this->getRequest()->getParam('exceptionFile'));
		$response = $this->rawFactory->create();
		if (file_exists($exceptionFile)) {
			$contents = $this->driverFile->fileGetContents($exceptionFile);
			return $response->setContents($contents);
		}

		return $response->setContents('<h1>File "' . $exceptionFile . '" doesn\'t exist.');
	}

	protected function _isAllowed()
	{
		return true;
	}
}

?>
