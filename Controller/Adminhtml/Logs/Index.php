<?php
namespace Magezone\LogViewer\Controller\Adminhtml\Logs;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magezone\LogViewer\Logger\Logger;

/**
 * Class Index
 * @package Magezone\LogViewer\Controller\Adminhtml\Logs
 */
class Index extends \Magento\Backend\App\Action
{

	/**
	 * @var Logger
	 */
	protected $logger;

	/**
	 * @var PageFactory
	 */
	protected $resultPageFactory;

	/**
	 * Constructor
	 *
	 * @param Context $context
	 * @param PageFactory $resultPageFactory
	 * @param Logger $logger
	 */
	public function __construct(
		Context $context,
		PageFactory $resultPageFactory,
		Logger $logger
	)
	{
		parent::__construct($context);
		$this->logger = $logger;
		$this->resultPageFactory = $resultPageFactory;
	}

	/**
	 * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
	 * @throws \Magento\Framework\Exception\FileSystemException
	 */
	public function execute()
	{
		return $resultPage = $this->resultPageFactory->create();
	}

	protected function _isAllowed()
	{
		return true;
	}
}

?>
