<?php
namespace Magezone\LogViewer\Controller\Adminhtml\Columns;

use Magento\Framework\App\Filesystem\DirectoryList;
use Tracy\BlueScreen;
use Tracy\Debugger;

/**
 * Class Index
 * @package Magezone\LogViewer\Controller\Adminhtml\Logs
 */
class Index extends \Magento\Backend\App\Action
{
	protected $resultPageFactory = false;

	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory
	)
	{
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}

	public function execute()
	{
		$resultPage = $this->resultPageFactory->create();
		$resultPage->getConfig()->getTitle()->prepend((__('Log Column Formatters')));

		return $resultPage;
	}
}

?>
