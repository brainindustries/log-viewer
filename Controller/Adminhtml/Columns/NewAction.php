<?php

namespace Magezone\LogViewer\Controller\Adminhtml\Columns;
use Magezone\LogViewer\Model\Log\Column;

/**
 * Class NewAction
 * @package Magezone\LogViewer\Controller\Adminhtml\Columns
 */
class NewAction extends \Magento\Backend\App\Action
{

	protected $columnFactory = null;

	protected $resultPageFactory = null;

	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Magezone\LogViewer\Model\Log\ColumnFactory $columnFactory
	)
	{
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
		$this->columnFactory = $columnFactory;
	}

	public function execute()
	{
		$columnData = $this->getRequest()->getParam('column');
//		echo $columnData;
//		print_r($this->getRequest()->getParams());die();
		if (is_array($columnData)) {

			$column = $this->columnFactory->create();
			$column->setData($columnData)->save();

			$resultRedirect = $this->resultRedirectFactory->create();
			return $resultRedirect->setPath('*/*/index');
		}

		$resultPage = $this->resultPageFactory->create();
		$resultPage->getConfig()->getTitle()->prepend((__('Log Column Formatters')));

		return $resultPage;
	}

}