<?php

namespace Magezone\LogViewer\Controller\Adminhtml\Columns;
use Magento\Backend\App\Action;
use Magezone\LogViewer\Model\Log\Column;

class Delete extends \Magento\Backend\App\Action
{

	protected $columnFactory = null;

	public function __construct(
		Action\Context $context,
		\Magezone\LogViewer\Model\Log\ColumnFactory $columnFactory
	)
	{
		parent::__construct($context);
		$this->columnFactory = $columnFactory;
	}

	public function execute()
	{
		$id = $this->getRequest()->getParam('id');
		$column = $this->columnFactory->create()->load($id);

		if (!$column) {
			$this->messageManager->addErrorMessage(__('Unable to proceed. Please, try again.'));
			$resultRedirect = $this->resultRedirectFactory->create();
			return $resultRedirect->setPath('*/*/index', array('_current' => true));
		}
		try{
			$column->delete();
			$this->messageManager->addSuccessMessage(__(' has been deleted !'));
		} catch (\Throwable $e) {
			$this->messageManager->addErrorMessage(__('Error while trying to delete log column formatter: '));
			$resultRedirect = $this->resultRedirectFactory->create();
			return $resultRedirect->setPath('*/*/index', array('_current' => true));
		}

		$resultRedirect = $this->resultRedirectFactory->create();
		return $resultRedirect->setPath('*/*/index', array('_current' => true));
	}
}