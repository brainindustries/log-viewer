<?php

namespace Magezone\LogViewer\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Tracy\Debugger;
use Magento\Framework\App\State;
use Magento\Developer\Helper\Data as DevHelper;

class TracyDebugStart implements ObserverInterface
{

	protected $appState;

	protected $devHelper;

	public function __construct(
		State $appState,
		DevHelper $devHelper
	)
	{
		$this->appState = $appState;
		$this->devHelper = $devHelper;
	}

	/**
	 * @param Observer $observer
	 * @return void
	 */
	public function execute(Observer $observer)
	{
		if ($this->appState->getMode() === State::MODE_DEVELOPER || $this->devHelper->isDevAllowed()) {
			Debugger::enable(Debugger::DEVELOPMENT);
		}
	}
}