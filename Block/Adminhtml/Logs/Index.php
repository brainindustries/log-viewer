<?php

namespace Magezone\LogViewer\Block\Adminhtml\Logs;

use Magento\Backend\Block\Template\Context;
use Magezone\LogViewer\Model\Config\Source\Logfiles;

class Index extends \Magento\Backend\Block\Template
{


	protected $logfiles;

	public function __construct(
		Context $context,
		array $data = [],
		Logfiles $logfiles
	)
	{
		parent::__construct($context, $data);
		$this->logfiles = $logfiles;
	}

	public function getLogsJson()
	{
		$logfiles = $this->logfiles->toOptionArray();
		return json_encode($logfiles);
	}

}