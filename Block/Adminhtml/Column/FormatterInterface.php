<?php

namespace Magezone\LogViewer\Block\Adminhtml\Column;

/**
 * Interface FormatterInterface
 * @package Magezone\LogViewer\Block\Adminhtml\Column
 */
interface FormatterInterface
{
	/**
	 * @param mixed $data
	 * @param $logName
	 * @return string
	 */
	public function serializeColumn($data, $logName);

	/**
	 * @param string $data
	 * @param $query
	 * @return mixed
	 */
	public function unserializeColumn($data, $query);
}