<?php

namespace Magezone\LogViewer\Block\Adminhtml\Column\Formatter;

use Magento\Framework\View\Element\AbstractBlock;
use Magezone\LogViewer\Block\Adminhtml\Column\FormatterInterface;
use Tracy\BlueScreen;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Io\File as IoFile;

class ExceptionData extends AbstractBlock implements FormatterInterface
{

	const DATA_SEPARATOR = '@@';

	/**
	 * @var DirectoryList
	 */
	protected $directoryList;

	/**
	 * @var IoFile
	 */
	protected $ioFile;

	public function __construct(
		\Magento\Framework\View\Element\Context $context,
		array $data = [],
		DirectoryList $directoryList,
		IoFile $ioFile
	)
	{
		parent::__construct($context, $data);
		$this->directoryList = $directoryList;
		$this->ioFile = $ioFile;
	}

	public function serializeColumn($data, $logName)
	{
		if ($data instanceof \Throwable) {
			$exceptionDir = $this->directoryList->getPath(DirectoryList::LOG) . DIRECTORY_SEPARATOR . $logName . '_exceptions';
			if (!file_exists($exceptionDir)) {
				$this->ioFile->mkdir($exceptionDir, 0775);
			}

			$fileName = md5(time() . rand()) . '.html';
			$filePath = $exceptionDir . DIRECTORY_SEPARATOR . $fileName;

			$blueScreen = new BlueScreen();
			$blueScreen->renderToFile($data, $filePath);
			return implode(self::DATA_SEPARATOR, [$data->getMessage(), $filePath]);
		} else {
			return (new StringData())->serializeColumn($data, $logName);
		}
	}

	public function unserializeColumn($data, $query)
	{
		list($message, $filePath) = explode(self::DATA_SEPARATOR, $data);
		if (file_exists($filePath)) {
			$message = (new StringData())->unserializeColumn($message, $query);
			$message = '<a href="' . $this->getUrl('logviewer/logs/exception', ['exceptionFile' => urlencode($filePath)]) . '" target="_blank">' . $message . '</a>';
		}
		return $message;
	}
}