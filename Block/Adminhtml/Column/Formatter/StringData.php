<?php

namespace Magezone\LogViewer\Block\Adminhtml\Column\Formatter;

use Magezone\LogViewer\Block\Adminhtml\Column\FormatterInterface;

class StringData implements FormatterInterface
{

	public function serializeColumn($data, $logName)
	{
		return (string)$data;
	}

	public function unserializeColumn($data, $query)
	{
		if (!empty($query)) {
			$query = preg_quote(trim($query));
			$regexp = "/($query)(?![^<]+>)/i";
			$replacement = '<b class="search_query">\\1</b>';
			$data = preg_replace($regexp, $replacement, $data);
		}

		return (string)$data;
	}
}