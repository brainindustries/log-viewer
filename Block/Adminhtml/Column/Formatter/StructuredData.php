<?php

namespace Magezone\LogViewer\Block\Adminhtml\Column\Formatter;

use Magezone\LogViewer\Block\Adminhtml\Column\FormatterInterface;
use Tracy\Debugger;

/**
 * Class StructuredData
 * @package Magezone\LogViewer\Block\Adminhtml\Column\Formatter
 */
class StructuredData implements FormatterInterface
{

	public function serializeColumn($data, $logName)
	{
		return json_encode($data);
	}

	public function unserializeColumn($data, $query)
	{
		$data = json_decode($data);
		$data = Debugger::dump($data, true);
		return (new StringData())->unserializeColumn($data, $query);
	}
}